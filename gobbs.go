package main

import (
	"database/sql"
	"fmt"
	"log"
	"net"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type BBSUser struct {
	ID       string
	Username string
	Password string
}

func login(un string) BBSUser {
	b := BBSUser{}
	database, _ := sql.Open("sqlite3", "../db/gobbs.db")
	q := "SELECT id,username,password FROM users where username='" + un + "'"
	rows, _ := database.Query(q)
	for rows.Next() {
		rows.Scan(&b.ID, &b.Username, &b.Password)
	}
	return b
}

func main() {

	ln, err := net.Listen("tcp", ":6000")
	if err != nil {
		log.Println(err)
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go handleConnection(conn)
	}

}

func handleConnection(c net.Conn) {
	// needs to be a state machine
	const telnetcmd1 = "\xff\xfe\x10"
	const telnetcmd2 = "\xff\xfb\x03"
	c.Write([]byte(telnetcmd1))
	time.Sleep(2 * time.Millisecond)
	c.Write([]byte(telnetcmd2))

	rc := c.RemoteAddr()
	cs := "Connected from " + rc.String() + "\n"
	c.Write([]byte(cs))

	var buf [1]byte
	var bb string
	for {
		_, err := c.Read(buf[0:])
		if err != nil {
			return
		}
		switch buf[0] {
		case 255:
			bb = "AIC"
		case 254:
			bb = "DONT"
		case 253:
			bb = "DO"
		case 252:
			bb = "WONT"
		case 251:
			bb = "WILL"
		case 250:
			bb = "SB"
		case 249:
			bb = "GA"
		case 248:
			bb = "EL"
		default:
			bb = string(buf[0:])
		}

		fmt.Printf("%s", bb)

		bc := "echo '" + bb + "' "
		_, err2 := c.Write([]byte(bc))
		if err2 != nil {
			return
		}
	}
}

/*
func SendWelcomeScreen( c net.Conn ) {
	database, _ := sql.Open("sqlite3", "../db/gobbs.db")
	q := "SELECT screenfile FROM screens WHERE ID = "
}
*/
